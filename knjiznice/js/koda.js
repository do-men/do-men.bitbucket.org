/* global L, btoa, ehrId */

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var mapa;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};


window.addEventListener('load', function () {
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };
  
  //Izdalava mape
  mapa = new L.map('mapa_id', mapOptions);
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
  mapa.addLayer(layer);
  var popup = L.popup();
  
  
  getJSON('https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json', function(err, data) {
    if (err !== null) {
      console.log('Something went wrong: ' + err);
    } else {
      //console.log(data.features[0].geometry.coordinates);
      L.geoJSON(data, {
        onEachFeature: function (feature, layer) {
          //if (feature.properties['addr:city'] && feature.properties['addr:street'] &&
          if (feature.properties["addr:city"] && feature.properties["addr:street"] && feature.properties["addr:housenumber"]){
            //console.log(feature.properties["addr:city"]);
            //console.log(feature.properties["addr:street"]);
            //console.log(feature.properties["addr:housenumber"]);
            layer.bindPopup(feature.properties["name"] +'<p>' + feature.properties["addr:street"] + " " + feature.properties["addr:housenumber"] + '</p>');
          }
        }
      }).addTo(mapa);
    }
  });
  
  
  
  
  
  
});


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
